package com.jonathanhult.webcenter.content.minify;

/**
*
* @author Jonathan Hult
* @version build_1_20120711
* Copyright <a href="http://jonathanhult.com">http://jonathanhult.com</a>
*
*/
public class MinifyConstants {
	public static final String UTF8 = "UTF-8";
	public static final String JS = "JS";
	public static final String CSS = "CSS";
	public static final String TRACE_SECTION = "minify";
	public static final String WEB_FILE_PATH = "WebfilePath";
	public static final String DOC_SERVICE_HANDLER  = "DocServiceHandler";
	public static final String COMPONENT_ENABLED = "Minify_ComponentEnabled";
}
