package com.jonathanhult.webcenter.content.minify;

import static com.jonathanhult.webcenter.content.minify.MinifyConstants.COMPONENT_ENABLED;
import static com.jonathanhult.webcenter.content.minify.MinifyConstants.CSS;
import static com.jonathanhult.webcenter.content.minify.MinifyConstants.JS;
import static com.jonathanhult.webcenter.content.minify.MinifyConstants.TRACE_SECTION;
import static com.jonathanhult.webcenter.content.minify.MinifyConstants.UTF8;
import static com.jonathanhult.webcenter.content.minify.MinifyConstants.WEB_FILE_PATH;
import static intradoc.common.FileUtils.closeObject;
import static intradoc.common.FileUtils.getExtension;
import static intradoc.common.FileUtils.openDataReader;
import static intradoc.common.FileUtils.openDataWriter;
import static intradoc.common.Report.m_verbose;
import static intradoc.shared.SharedObjects.getEnvValueAsBoolean;
import intradoc.common.ExecutionContext;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.shared.FilterImplementor;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

/**
 *
 * @author Jonathan Hult
 * @version build_1_20120711
 * Copyright <a href="http://jonathanhult.com">http://jonathanhult.com</a>
 *
 */
public class MinifyPostWebfileCreationFilter implements FilterImplementor {

	/**
	 * This method implements a "postWebfileCreation" Java filter to take the web viewable (or alternate) file and if it is a JavaScript or CSS file, use YUICompressor to minify the file. The primary file is not touched.
	 * @param workspace Workspace from filter.
	 * @param binder DataBinder from filter.
	 * @param context ExecutionContext from filter.
	 */
	public int doFilter(final Workspace workspace, final DataBinder binder, final ExecutionContext context)
			throws DataException, ServiceException {
		traceVerbose("Start doFilter for MinifyPostWebfileCreationFilter");

		try {
			if (!getEnvValueAsBoolean(COMPONENT_ENABLED, false)) {
				trace("Component not enabled; will not minify");
				return CONTINUE;
			}

			// Retrieve web/alternate file path
			final String webfilePath = binder.getLocal(WEB_FILE_PATH);
			if (webfilePath == null || webfilePath.trim().length() < 1) {
				trace("WebfilePath was missing; will not minify");
				return CONTINUE;
			}
			traceVerbose("WebfilePath: " + webfilePath);

			// Retrieve extension for web/alternate file
			final String extension = getExtension(webfilePath);
			if (extension == null || extension.trim().length() < 1) {
				trace("Extension was missing for WebfilePath; will not minify");
				return CONTINUE;
			}
			traceVerbose("extension: " + extension);

			// Exit if extension is not JS nor CSS
			if (!extension.equalsIgnoreCase(JS) && !extension.equalsIgnoreCase(CSS)) {
				traceVerbose("Extension was not " + JS + " nor " + CSS + "; will not minify");
				return CONTINUE;
			}
			trace("Found " + JS + " or " + CSS + " extension; will attempt to minify web file");

			// Do the actual compression
			doCompression(webfilePath, extension);

			return CONTINUE;
		} finally {
			traceVerbose("End doFilter for MinifyPostWebfileCreationFilter");
		}
	}

	/**
	 * This method calls the appropriate compression method whether for CSS or JS.
	 * @param webfilePath The path of the file to compress.
	 * @param extension The extension of the file to compress. This should be either CSS or JS.
	 */
	private void doCompression(final String webfilePath, final String extension) {
		traceVerbose("Start doCompression");

		try {
			// File reader
			final Reader in = openDataReader(new File(webfilePath), UTF8);

			// File writer
			final Writer out = null;

			try {
				if (extension.equalsIgnoreCase(JS)) {
					trace("Attemping JS compression");
					doJSCompression(in, out, webfilePath);
				} else if (extension.equalsIgnoreCase(CSS)) {
					trace("Attemping CSS compression");
					doCSSCompression(in, out, webfilePath);
				}
				trace("Successfully compressed " + webfilePath);
			} catch (final EvaluatorException e) {
				warn("Something went wrong during minifying " + webfilePath + ".", e);
			} catch (final IOException e) {
				warn("Something went wrong during minifying " + webfilePath + ".", e);
			} finally {
				closeObject(in);
			}
		} catch (final IOException e) {
			warn("Could not read file " + webfilePath + ".", e);
		} finally {
			traceVerbose("End doCompression");
		}
	}

	/**
	 * This method compresses CSS.
	 * @param in Reader which is the input file.
	 * @param out Writer which is the output file.
	 * @param outputFilename The filename for the output file.
	 * @throws IOException
	 */
	private void doCSSCompression(final Reader in, Writer out, final String outputFilename) throws IOException {
		traceVerbose("Start doCSSCompression");

		try {
			// CSS compressor
			final CssCompressor compressor = new CssCompressor(in);

	        // Close the input stream first, and then open the output stream,
	        // in case the output file should override the input file
	        closeObject(in);

	        // Get Writer object for output file
	        out = openDataWriter(new File(outputFilename), UTF8);

	        // Compress file
	        compressor.compress(out, -1);
		} finally {
			closeObject(out);
			traceVerbose("End doCSSCompression");
		}
	}

	/**
	 * This method compresses JavaScript.
	 * @param in Reader which is the input file.
	 * @param out Writer which is the output file.
	 * @param outputFilename The filename for the output file.
	 * @throws EvaluatorException
	 * @throws IOException
	 */
	private void doJSCompression(final Reader in, Writer out, final String outputFilename) throws EvaluatorException, IOException {
		traceVerbose("Start doJSCompression");

		try {
			// JS compressor
			final JavaScriptCompressor compressor = new JavaScriptCompressor(in, new ErrorReporter() {

		         public void warning(final String message, final String sourceName,
		                 final int line, final String lineSource, final int lineOffset) {
		             if (line < 0) {
		                 trace(message);
		             } else {
		                 trace(line + ':' + lineOffset + ':' + message);
		             }
		         }

		         public void error(final String message, final String sourceName,
		                 final int line, final String lineSource, final int lineOffset) {
		        	 warning(message, sourceName, line, lineSource, lineOffset);
		         }

		         public EvaluatorException runtimeError(final String message, final String sourceName,
		                 final int line, final String lineSource, final int lineOffset) {
		             error(message, sourceName, line, lineSource, lineOffset);
		             return new EvaluatorException(message);
		         }
			});

	         // Close the input stream first, and then open the output stream,
	         // in case the output file should override the input file
			 closeObject(in);

	         // Get Writer object for output file
	         out = openDataWriter(new File(outputFilename), UTF8);

	         // Compress file
	         compressor.compress(out, -1, true, false,
	                 true, false);
		} finally {
			closeObject(out);
			traceVerbose("End doJSCompression");
		}
	}

	/**
	 * This method traces output to the UCM console.
	 * @param message The String to trace to the UCM console.
	 */
	public void trace(final String message) {
		Report.trace(TRACE_SECTION, message, null);
	}

	/**
	 * This method verbose traces output to the UCM console.
	 * @param message The String to trace to the UCM console.
	 */
	public void traceVerbose(final String message) {
		if (m_verbose) {
			trace(message);
		}
	}

	/**
	 * This method logs output to the UCM log.
	 * @param message The String to log to the UCM log.
	 */
	public void warn(final String message, final Exception exception) {
		Report.trace(TRACE_SECTION, message, exception);
	}
}
