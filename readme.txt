﻿Minify Component

*******************************************************************************
** Component Information
*******************************************************************************

	Component:              Minify
	Author:                 Jonathan Hult
	Website:                http://jonathanhult.com
	Last Updated On:        build_1_20120711

*******************************************************************************
** Overview
*******************************************************************************

	This component uses a "postWebfileCreation" Java filter to take the web 
	viewable (or alternate) file and if it is a JavaScript or CSS file, use 
	YUI Compressor to minify the file. The primary file is not touched. This 
	component works on a day-forward basis meaning CSS and JavaScript files 
	which have been checked-in prior to enabling this component, will not be 
	minified. If past files need to be minified, they can be submitted for 
	reconversion using Repository Manager.
	
	Preference Prompt:
	Minify_ComponentEnabled - Boolean which allows enabling/disabling 
	minification without requiring a restart of the WebCenter Content server
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	
	- 10.1.3.5.1 (111229) (Build: 7.2.4.105) 

*******************************************************************************
** HISTORY
*******************************************************************************

	build_1_20120711
		- Initial component release